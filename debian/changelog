php-horde-cssminify (1.0.4-7) UNRELEASED; urgency=medium

  * d/watch: Switch to format version 4.
  * d/control: Bump Standards-Version: to 4.6.2. No changes needed.

 -- Mike Gabriel <sunweaver@debian.org>  Thu, 19 Jan 2023 08:56:22 +0100

php-horde-cssminify (1.0.4-6) unstable; urgency=medium

  [ Mike Gabriel ]
  * d/control: Add to Uploaders: Juri Grabowski.
  * d/control: Bump DH compat level to version 13.

  [ Juri Grabowski ]
  * d/salsa-ci.yml: enable aptly

 -- Mike Gabriel <sunweaver@debian.org>  Wed, 01 Jul 2020 10:33:21 +0200

php-horde-cssminify (1.0.4-5) unstable; urgency=medium

  * Re-upload to Debian. (Closes: #959251).

  * d/control: Add to Uploaders: Mike Gabriel.
  * d/control: Drop from Uploaders: Debian QA Group.
  * d/control: Bump Standards-Version: to 4.5.0. No changes needed.
  * d/control: Add Rules-Requires-Root: field and set it to 'no'.
  * d/copyright: Update copyright attributions.
  * d/upstream/metadata: Add file. Comply with DEP-12.

 -- Mike Gabriel <sunweaver@debian.org>  Mon, 04 May 2020 21:18:17 +0200

php-horde-cssminify (1.0.4-4) unstable; urgency=medium

  * Bump debhelper from old 11 to 12.
  * d/control: Orphaning package (See #942282)

 -- Mathieu Parent <sathieu@debian.org>  Fri, 18 Oct 2019 18:52:12 +0200

php-horde-cssminify (1.0.4-3) unstable; urgency=medium

  * Update Standards-Version to 4.1.4, no change
  * Update Maintainer field

 -- Mathieu Parent <sathieu@debian.org>  Tue, 15 May 2018 00:06:18 +0200

php-horde-cssminify (1.0.4-2) unstable; urgency=medium

  * Update Standards-Version to 4.1.3, no change
  * Upgrade debhelper to compat 11
  * Update Vcs-* fields
  * Use secure copyright format URI
  * Replace "Priority: extra" by "Priority: optional"

 -- Mathieu Parent <sathieu@debian.org>  Thu, 05 Apr 2018 11:24:24 +0200

php-horde-cssminify (1.0.4-1) unstable; urgency=medium

  * New upstream version 1.0.4

 -- Mathieu Parent <sathieu@debian.org>  Tue, 26 Sep 2017 21:43:36 +0200

php-horde-cssminify (1.0.3-1) unstable; urgency=medium

  * New upstream version 1.0.3

 -- Mathieu Parent <sathieu@debian.org>  Sat, 17 Dec 2016 21:48:23 +0100

php-horde-cssminify (1.0.2-6) unstable; urgency=medium

  * Update Standards-Version to 3.9.8, no change
  * Updated d/watch to use https

 -- Mathieu Parent <sathieu@debian.org>  Tue, 07 Jun 2016 22:18:41 +0200

php-horde-cssminify (1.0.2-5) unstable; urgency=medium

  * Update Standards-Version to 3.9.7, no change
  * Use secure Vcs-* fields
  * Rebuild with newer pkg-php-tools for the PHP 7 transition

 -- Mathieu Parent <sathieu@debian.org>  Sat, 12 Mar 2016 22:40:41 +0100

php-horde-cssminify (1.0.2-4) unstable; urgency=medium

  * Upgaded to debhelper compat 9

 -- Mathieu Parent <sathieu@debian.org>  Sat, 24 Oct 2015 15:15:48 +0200

php-horde-cssminify (1.0.2-3) unstable; urgency=medium

  * Update gbp.conf

 -- Mathieu Parent <sathieu@debian.org>  Sun, 09 Aug 2015 22:21:06 +0200

php-horde-cssminify (1.0.2-2) unstable; urgency=medium

  * Update Standards-Version to 3.9.6, no change

 -- Mathieu Parent <sathieu@debian.org>  Mon, 04 May 2015 20:17:40 +0200

php-horde-cssminify (1.0.2-1) unstable; urgency=medium

  * New upstream version 1.0.2

 -- Mathieu Parent <sathieu@debian.org>  Fri, 03 Oct 2014 22:10:16 +0200

php-horde-cssminify (1.0.1-2) unstable; urgency=medium

  * Update Standards-Version, no change
  * Update Vcs-Browser to use cgit instead of gitweb

 -- Mathieu Parent <sathieu@debian.org>  Sun, 24 Aug 2014 09:26:32 +0200

php-horde-cssminify (1.0.1-1) unstable; urgency=medium

  * New upstream version 1.0.1

 -- Mathieu Parent <sathieu@debian.org>  Tue, 15 Jul 2014 23:18:45 +0200

php-horde-cssminify (1.0.0-1) unstable; urgency=low

  * Horde's Horde_CssMinify package
  * Initial packaging (Closes: #748435)

 -- Mathieu Parent <sathieu@debian.org>  Sat, 17 May 2014 10:44:19 +0200
